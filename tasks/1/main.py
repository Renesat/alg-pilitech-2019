from pathlib import Path


class Node():
    def __init__(self, pos, valid=0):
        self.visited = False
        self.path_size = None
        self.parent = None
        self.pos = pos
        self.valid = valid


def return_path(node):
    path = []
    cnode = node
    while cnode is not None:
        path.append(cnode.pos)
        cnode = cnode.parent
    return path


def find_path(start_pos, matrix):
    node_matrix = [[Node((x, y), valid=l[x]) for x in range(len(l))]
                   for y, l in enumerate(matrix)]
    stack = [node_matrix[start_pos[1]][start_pos[0]]]
    exits = []
    while stack:
        node = stack.pop()
        if node.path_size is None:
            node.path_size = 0
        node.visited = True
        if node.valid == 1:
            continue
        if (node.pos[1] >= len(lab_matrix) - 1 or node.pos[1] <= 0
                or node.pos[0] <= 0 or node.pos[0] >= len(lab_matrix[0]) - 1):
            exits.append(node)
            continue
        else:
            nb_pos = [(node.pos[0] - dx, node.pos[1] - dy)
                      for dx, dy in [[-1, 0], [1, 0], [0, -1], [0, 1]]]
            nb_node = [node_matrix[pos[1]][pos[0]] for pos in nb_pos]
            for next_node in nb_node:
                if not next_node.visited:
                    if (next_node.path_size is None
                            or next_node.path_size > node.path_size + 1):
                        next_node.path_size = node.path_size + 1
                        next_node.parent = node
                    if next_node not in stack:
                        stack.append(next_node)
    return exits[0]


if __name__ == "__main__":
    ifile = Path('input.txt')

    lab_matrix = []
    with open(ifile) as f:
        start_pos = [int(x) for x in next(f).split(' ')]
        for line in f:
            lab_matrix.append([int(x) for x in line.split(' ')])
    print(return_path(find_path(start_pos, lab_matrix)))
