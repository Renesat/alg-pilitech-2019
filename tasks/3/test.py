#!/usr/bin/env python

import main


def is_topological_list(lst, graph):
    for item, neighbours in graph.items():
        for neighbour in neighbours:
            if lst.index(neighbour) < lst.index(item):
                return False
    return True


def test_graph1():
    graph = {
        '555': ['1', '2'],
        '1': ['2'],
        '2': ['3', '4'],
    }

    assert is_topological_list(main.topological_sort(graph), graph)


def test_graph2():
    graph = {'555': ['1', '2'], '1': ['2'], '2': ['3', '4'], '4': ['1']}

    assert main.topological_sort(graph) is None
