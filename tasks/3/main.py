#!/usr/bin/env python

import sys
from pathlib import Path


class Heandler():
    def __init__(self, data, heandlers):
        self.data = data
        self.heandlers = heandlers

    def run(self, **kwargs):
        for heandler in self.heandlers:
            self.data = heandler(self.data, **kwargs)


def find_sources(graph):
    sources = set(graph)
    for item, neighbours in graph.items():
        for neighbour in neighbours:
            if neighbour in sources:
                sources.remove(neighbour)
    return sources


def dfs(graph, item, visited=None, heandler=None):
    if visited is None:
        visited = set()
    visited.add(item)

    if heandler is not None:
        heandler.run(state='bn', item=item, visited=visited)

    for neighbour in graph.get(item, []):

        if heandler is not None:
            heandler.run(state='n',
                         item=item,
                         neighbour=neighbour,
                         visited=visited)

        if neighbour in visited:
            continue
        dfs(graph, neighbour, visited=visited, heandler=heandler)

    if heandler is not None:
        heandler.run(state='an', item=item, visited=visited)


def topological_sort(graph):
    if is_cycle_graph(graph):
        return None
    visited = set()
    heandler = Heandler(
        [],
        [lambda data, **k: data + [k['item']] if k['state'] == 'an' else data])
    for item in find_sources(graph):
        dfs(graph, item, visited, heandler)
    return list(reversed(heandler.data))


def is_cycle_graph(graph):
    def cycle_header(data, **k):
        is_cycle, fvisited = data
        if is_cycle:
            return data
        neighbour = k.get('neighbour', None)
        new_is_cycle = neighbour not in fvisited and neighbour in k['visited']
        if k['state'] == 'an':
            new_fvisited = fvisited.union([k['item']])
        else:
            new_fvisited = fvisited
        return new_is_cycle, new_fvisited

    visited = set()
    heandler = Heandler((False, set()), [cycle_header])
    for item in find_sources(graph):
        dfs(graph, item, visited, heandler)
        if heandler.data[0]:
            break
    return heandler.data[0]


def main():
    if len(sys.argv) <= 1:
        print("Укажите файл для чтения")
        exit()

    ifile = Path(sys.argv[1])

    graph = {}
    with open(ifile) as f:
        for line in f:
            u, v = line.split('->')
            if graph.get(u.strip(), None) is None:
                graph[u.strip()] = [v.strip()]
            else:
                graph[u.strip()].append(v.strip())

    print(topological_sort(graph))


if __name__ == "__main__":
    main()
