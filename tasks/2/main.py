#!/usr/bin/env python

import sys
from dataclasses import dataclass
from pathlib import Path


@dataclass(frozen=True)
class Point():
    x: int
    y: int


def matrix_size(matrix):
    return len(matrix), len(matrix[0])


def get_value(matrix, point):
    return matrix[point.y][point.x]


def point_neighbours(point):
    x = point.x
    y = point.y
    return [
        Point(nx, ny)
        for nx, ny in [[x - 1, y], [x + 1, y], [x, y + 1], [x, y - 1]]
    ]


def in_box(point, height, width):
    return (point.y < height and point.x < width and point.y >= 0
            and point.x >= 0)


def find_island(start, imap):
    points = [start]
    island = set()
    height, width = matrix_size(imap)
    while points:
        point = points.pop()
        for npoint in point_neighbours(point):
            if in_box(npoint, height, width) and npoint not in island:
                if get_value(imap, point) == 1:
                    points.append(npoint)
                    island.add(npoint)
    return island


def find_islands(imap):
    islands = []
    visited = set()
    height, width = matrix_size(imap)
    for y in range(height):
        for x in range(width):
            point = Point(x, y)
            if get_value(imap, point) == 1 and point not in visited:
                island = find_island(point, imap)
                islands.append(island)
                visited = visited.union(island)
    return islands


def main():
    if len(sys.argv) <= 1:
        print("Укажите файл для чтения")
        exit()

    ifile = Path(sys.argv[1])

    matrix = []
    with open(ifile) as f:
        for line in f:
            matrix.append([int(x) for x in line.split()])
    print(len(find_islands(matrix)))


if __name__ == "__main__":
    main()
