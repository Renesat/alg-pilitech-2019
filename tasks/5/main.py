#!/usr/bin/env python

import re
import sys
from dataclasses import dataclass
from pathlib import Path


@dataclass(frozen=True)
class Edge():
    frm: str
    to: str
    value: int


def add_edge(graph, edge):
    v = edge.frm
    u = edge.to
    value = edge.value
    if graph.get(u, None) is None:
        graph[u] = [(v, value)]
    else:
        graph[u].append((v, value))
    if graph.get(v, None) is None:
        graph[v] = [(u, value)]
    else:
        graph[v].append((u, value))


def del_edge(graph, edge):
    v = edge.frm
    u = edge.to
    for i, node in enumerate(graph.get(u, [])):
        if node[0] == v:
            del graph[u][i]
            break
    for i, node in enumerate(graph.get(v, [])):
        if node[0] == u:
            del graph[v][i]
            break


def get_graph(edges):
    graph = {}
    for edge in edges:
        v = edge.frm
        u = edge.to
        value = edge.value
        if graph.get(u, None) is None:
            graph[u] = [(v, value)]
        else:
            graph[u].append((v, value))
        if graph.get(v, None) is None:
            graph[v] = [(u, value)]
        else:
            graph[v].append((u, value))
    return graph


def item_in_cycle(graph, item, visited=None, visited_edges=None):
    if visited is None:
        visited = set()
        visited_edges = set()
    visited.add(item)

    res_value = False
    for neighbour, _ in graph.get(item, []):
        if ((item, neighbour) in visited_edges
                or (neighbour, item) in visited_edges):
            continue
        visited_edges.add((item, neighbour))
        if neighbour in visited:
            return True
        res_value = res_value or item_in_cycle(
            graph, neighbour, visited=visited, visited_edges=visited_edges)
    return res_value


def kruscal(edges):
    ostov_graph = {}
    for edge in sorted(edges, key=lambda x: x.value):
        add_edge(ostov_graph, edge)
        if item_in_cycle(ostov_graph, edge.frm):
            del_edge(ostov_graph, edge)
    return ostov_graph


def main():
    if len(sys.argv) <= 1:
        print("Укажите файл для чтения")
        exit()

    ifile = Path(sys.argv[1])

    edges = []
    with open(ifile) as f:
        for line in f:
            edgeRegex = re.compile(
                r'^\s*([^\s]+)\s*--\s*([^\s]+)\s*:\s*([^\s]+)\s*$')
            u, v, value = edgeRegex.match(line).group(1, 2, 3)
            edges.append(Edge(u, v, int(value)))

    print(kruscal(edges))


if __name__ == "__main__":
    main()
