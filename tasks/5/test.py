#!/usr/bin/env python

import main


def eq_graph(graph1, graph2):
    for node, value in graph2.items():
        if set(graph1.get(node, [])) != set(value):
            return False
    return True


def test_graph1():
    edges = [
        main.Edge("A", "B", 2),
        main.Edge("B", "C", 5),
        main.Edge("B", "D", 2),
        main.Edge("C", "D", 4),
        main.Edge("C", "E", 2),
        main.Edge("D", "E", 3),
        main.Edge("D", "F", 7),
        main.Edge("E", "F", 8)
    ]

    res = main.kruscal(edges)

    real_edges = [
        main.Edge("A", "B", 2),
        main.Edge("B", "D", 2),
        main.Edge("C", "E", 2),
        main.Edge("D", "E", 3),
        main.Edge("D", "F", 7)
    ]
    real_graph = main.get_graph(real_edges)

    assert eq_graph(res, real_graph)


def test_graph2():
    edges = [
        main.Edge("A", "B", 2),
        main.Edge("B", "D", 5),
        main.Edge("B", "E", 1),
        main.Edge("B", "C", 3),
        main.Edge("E", "G", 8),
        main.Edge("E", "F", 9),
        main.Edge("F", "C", 2),
        main.Edge("F", "H", 1),
        main.Edge("G", "H", 2),
        main.Edge("G", "J", 10),
        main.Edge("H", "K", 1),
        main.Edge("K", "I", 2),
        main.Edge("I", "J", 4),
        main.Edge("I", "G", 6)
    ]

    res = main.kruscal(edges)

    real_edges = [
        main.Edge("A", "B", 2),
        main.Edge("B", "D", 5),
        main.Edge("B", "E", 1),
        main.Edge("B", "C", 3),
        main.Edge("F", "C", 2),
        main.Edge("F", "H", 1),
        main.Edge("G", "H", 2),
        main.Edge("H", "K", 1),
        main.Edge("K", "I", 2),
        main.Edge("I", "J", 4),
    ]
    real_graph = main.get_graph(real_edges)

    assert eq_graph(res, real_graph)
