#!/usr/bin/env python

import sys
from pathlib import Path


class Handler():
    def __init__(self, data, handlers):
        self.data = data
        self.handlers = handlers

    def run(self, **kwargs):
        for handler in self.handlers:
            self.data = handler(self.data, **kwargs)


class Graph():
    def __init__(self, edges):
        self.edges = edges

    def graph(self):
        graph = {}
        for u, v in self.edges:
            if graph.get(u, None) is None:
                graph[u] = [v]
            else:
                graph[u].append(v)
        return graph

    def graph_T(self):
        graph = {}
        for u, v in self.edges:
            if graph.get(v, None) is None:
                graph[v] = [u]
            else:
                graph[v].append(u)
        return graph


def find_sources(graph):
    sources = set(graph)
    for item, neighbours in graph.items():
        for neighbour in neighbours:
            if neighbour in sources:
                sources.remove(neighbour)
    return sources


def dfs(graph, item, visited=None, handler=None):
    if visited is None:
        visited = set()
    visited.add(item)

    if handler is not None:
        handler.run(state='bn', item=item, visited=visited)

    for neighbour in graph.get(item, []):

        if handler is not None:
            handler.run(state='n',
                        item=item,
                        neighbour=neighbour,
                        visited=visited)

        if neighbour in visited:
            continue
        dfs(graph, neighbour, visited=visited, handler=handler)

    if handler is not None:
        handler.run(state='an', item=item, visited=visited)


def graph_sort(graph):
    visited = set()
    handler = Handler(
        [],
        [lambda data, **k: data + [k['item']] if k['state'] == 'an' else data])
    for item in find_sources(graph):
        dfs(graph, item, visited, handler)
    return list(reversed(handler.data))


def find_components(graph):
    seq = graph_sort(graph.graph())
    visited = set()

    def handler_def():
        return Handler([], [
            lambda data, **k: data + [k['item']]
            if k['state'] == 'an' else data
        ])

    components = []
    for item in seq:
        if item not in visited:
            handler = handler_def()
            dfs(graph.graph_T(), item, visited, handler)
            components.append(handler.data)
    return components


def main():
    if len(sys.argv) <= 1:
        print("Укажите файл для чтения")
        exit()

    ifile = Path(sys.argv[1])

    edges = []
    with open(ifile) as f:
        for line in f:
            u, v = line.split('->')
            edges.append((u.strip(), v.strip()))
    graph = Graph(edges)

    components = find_components(graph)
    print(components)


if __name__ == "__main__":
    main()
