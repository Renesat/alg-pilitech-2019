#!/usr/bin/env python

import main


def is_component(component, graph):
    def handler_def():
        return main.Handler(set(component), [
            lambda data, **k: data - set([k['item']])
            if k['state'] == 'an' else data
        ])

    for item in component:
        handler = handler_def()
        main.dfs(graph, item, set(), handler)
        if set(component) - handler.data == set():
            return False
    return True


def test_graph1():
    edges = [("A", "B"), ("B", "D"), ("B", "E"), ("B", "C"), ("E", "B"),
             ("E", "G"), ("E", "F"), ("F", "C"), ("F", "H"), ("C", "F"),
             ("G", "H"), ("G", "J"), ("H", "K"), ("K", "L"), ("L", "J"),
             ("J", "I"), ("I", "G")]
    graph = main.Graph(edges)

    for component in main.find_components(graph):
        assert is_component(component, graph.graph())


def test_graph2():
    edges = [("555", "1"), ("555", "2"), ("1", "2"), ("2", "3"), ("2", "4")]

    graph = main.Graph(edges)

    for component in main.find_components(graph):
        assert is_component(component, graph.graph())


def test_graph3():
    edges = [("555", "1"), ("555", "2"), ("1", "2"), ("2", "3"),
             ("2", "4")] + [("A", "B"), ("B", "D"), ("B", "E"), ("B", "C"),
                            ("E", "B"), ("E", "G"), ("E", "F"), ("F", "C"),
                            ("F", "H"), ("C", "F"), ("G", "H"), ("G", "J"),
                            ("H", "K"), ("K", "L"), ("L", "J"), ("J", "I"),
                            ("I", "G")]

    graph = main.Graph(edges)

    for component in main.find_components(graph):
        assert is_component(component, graph.graph())
