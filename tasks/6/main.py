#!/usr/bin/env python

import math
import random
import re
import sys
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path


@dataclass(frozen=True)
class Edge():
    frm: str
    to: str
    value: int


def get_graph(edges):
    G = defaultdict(list)
    w = defaultdict(dict)
    for edge in edges:
        u = edge.frm
        v = edge.to
        value = edge.value

        G[int(u)].append(int(v))
        w[int(u)][int(v)] = int(value)

        G[int(v)].append(int(u))
        w[int(v)][int(u)] = int(value)
    return G, w


class BST():
    def __init__(self, data=None, maximum=True):
        if data is None:
            data = []
        self.maximum = maximum
        self.__comp = max if self.maximum else min
        self.ids = {}
        self.tree = []
        self.end = len(data) - 1
        for val in data:
            self.insert(val)

    def get_root(self):
        if self.end < 0:
            return None
        return self.tree[0]

    def extract_root(self):
        if self.end < 0:
            return None
        value = self.tree[0]
        del self.ids[self.__get_name(0)]
        if self.end > 0:
            self.ids[self.__get_name(self.end)] = 0
            self.tree[0] = self.tree[self.end]
            self.__shift_down(0)
        self.end -= 1
        return value

    def insert(self, value):
        _, name = value
        if name is self.ids:
            raise ValueError('name exist')
        self.end += 1
        if self.end >= len(self.tree):
            self.tree += [None for _ in range(5)]
        self.ids[name] = self.end
        self.tree[self.end] = value
        self.__shift_up(self.end)

    def change_priority(self, value):
        prior, name = value
        item = self.ids.get(name, None)
        if item is None:
            raise ValueError('name not exist')
        old_prior, _ = self.tree[item]
        self.tree[item] = value
        comp_value = self.__comp(old_prior, prior)
        if prior == comp_value:
            self.__shift_up(item)
        else:
            self.__shift_down(item)

    def __get_priority(self, item):
        return self.tree[item][0]

    def __get_name(self, item):
        return self.tree[item][1]

    def __swap(self, item1, item2):
        name1 = self.__get_name(item1)
        name2 = self.__get_name(item2)
        self.ids[name1], self.ids[name2] = (self.ids[name2], self.ids[name1])
        self.tree[item1], self.tree[item2] = (self.tree[item2],
                                              self.tree[item1])

    def __parent(self, item):
        return (item - 1) // 2

    def __left_child(self, item):
        return (item + 1) * 2 - 1

    def __right_child(self, item):
        return (item + 1) * 2

    def __shift_up(self, item):
        if item <= 0:
            return
        par_item = self.__parent(item)
        next_item = self.__comp(item, par_item, key=self.__get_priority)
        if next_item == item:
            self.__swap(par_item, item)
            self.__shift_up(par_item)

    def __shift_down(self, item):
        next_item = item
        left_item = self.__left_child(item)
        right_item = self.__right_child(item)
        if left_item <= self.end:
            next_item = self.__comp(left_item,
                                    next_item,
                                    key=self.__get_priority)
        if right_item <= self.end:
            next_item = self.__comp(right_item,
                                    next_item,
                                    key=self.__get_priority)
        if item != next_item:
            self.__swap(item, next_item)
            self.__shift_down(next_item)

    def __iter__(self):
        for name in self.ids:
            yield name


def prim(G, w):
    cost = {}
    prev = {}
    H = BST(maximum=False)
    for v in G:
        cost[v] = math.inf
        prev[v] = None
        H.insert((cost[v], v))
    v0 = random.choice(list(G.keys()))
    cost[v0] = 0
    H.change_priority((cost[v0], v0))
    edges = []
    while H.ids:
        _, v = H.extract_root()
        for z in G[v]:
            if z in H and cost[z] >= w[v][z]:
                cost[z] = w[v][z]
                prev[z] = v
                H.change_priority((cost[z], z))
        if prev[v] is not None:
            edges.append((prev[v], v))
    # Create graph
    new_G = defaultdict(list)
    for v, u in edges:
        new_G[int(u)].append(int(v))
        new_G[int(v)].append(int(u))
    return new_G


def main():
    if len(sys.argv) <= 1:
        print("Укажите файл для чтения")
        exit()

    ifile = Path(sys.argv[1])

    edges = []
    with open(ifile) as f:
        for line in f:
            edgeRegex = re.compile(
                r'^\s*([^\s]+)\s*--\s*([^\s]+)\s*:\s*([^\s]+)\s*$')
            u, v, value = edgeRegex.match(line).group(1, 2, 3)
            edges.append(Edge(int(u), int(v), float(value)))
    G, w = get_graph(edges)
    print(prim(G, w))


if __name__ == "__main__":
    main()
