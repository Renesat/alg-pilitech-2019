#!/usr/bin/env python

import random
import unittest
from queue import PriorityQueue

from .main import BST, Edge, get_graph, prim


class TestBST(unittest.TestCase):
    def setUp(self):
        self.max_bst = BST(maximum=True)
        self.min_bst = BST(maximum=False)

    def test_random_example(self):
        N = 100
        examples = [
            set(
                random.randint(-100, 100)
                for _ in range(random.randint(1, 100))) for _ in range(N)
        ]
        for i, example in enumerate(examples):
            for maximum in [True, False]:
                with self.subTest(i=i, example=example, maximum=maximum):
                    que = PriorityQueue()
                    bst = BST(maximum=maximum)
                    for name, item in enumerate(example):
                        if not maximum:
                            que.put((item, name))
                        else:
                            que.put((-item, name))
                        bst.insert((item, name))
                    for _ in range(len(example)):
                        self.assertEqual(bst.extract_root()[1], que.get()[1])


class TestPrim(unittest.TestCase):
    def __eq_graph(self, graph1, graph2):
        for node, value in graph2.items():
            if set(graph1.get(node, [])) != set(value):
                return False
        return True

    def __test_template(self, edges, real_edges):
        G, w = get_graph(edges)
        res = prim(G, w)
        real_graph, _ = get_graph(real_edges)
        self.assertTrue(self.__eq_graph(res, real_graph))

    def test_graph1(self):
        edges = [
            Edge(1, 2, 2),
            Edge(2, 3, 5),
            Edge(2, 4, 2),
            Edge(3, 4, 4),
            Edge(3, 5, 2),
            Edge(4, 5, 3),
            Edge(4, 6, 7),
            Edge(5, 6, 8)
        ]
        real_edges = [
            Edge(1, 2, 2),
            Edge(2, 4, 2),
            Edge(3, 5, 2),
            Edge(4, 5, 3),
            Edge(4, 6, 7)
        ]
        self.__test_template(edges, real_edges)

    def test_graph2(self):
        edges = [
            Edge(1, 2, 2),
            Edge(2, 4, 5),
            Edge(2, 5, 1),
            Edge(2, 3, 3),
            Edge(5, 7, 8),
            Edge(5, 6, 9),
            Edge(6, 3, 2),
            Edge(6, 8, 1),
            Edge(7, 8, 2),
            Edge(7, 9, 10),
            Edge(8, 10, 1),
            Edge(10, 11, 2),
            Edge(11, 9, 4),
            Edge(11, 7, 6)
        ]
        real_edges = [
            Edge(1, 2, 2),
            Edge(2, 4, 5),
            Edge(2, 5, 1),
            Edge(2, 3, 3),
            Edge(6, 3, 2),
            Edge(6, 8, 1),
            Edge(7, 8, 2),
            Edge(8, 10, 1),
            Edge(10, 11, 2),
            Edge(11, 9, 4),
        ]
        self.__test_template(edges, real_edges)


if __name__ == '__main__':
    unittest.main()
